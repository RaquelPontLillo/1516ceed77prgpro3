package controlador;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Curso;
import modelo.IModelo;
import modelo.Matricula;
import vista.IVista;
import vista.VCurso;
import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CCurso {
    private IModelo modelo;
    
    public CCurso(IModelo m) {
        modelo = m;
        Boolean bucle = true;
        do {
            char opcion = VPrincipal.menuCrud();
            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();
                    return;
                case 'c':
                    create();
                    break;
                case 'r':
                    read();
                    break;
                case 'u':
                    update();
                    break;
                case 'd':
                    delete();
                    break;
                case 's':
                    search();
                    break;
                default:
                    VPrincipal.errorMenu();
                    break;
            }
        } while (bucle);
    }
       
    private void create() {      
        IVista<Curso> vc = new VCurso();
        Curso curso = vc.alta();
        HashSet cursos = modelo.readCurso();
        Iterator iterator = cursos.iterator();
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (curso.getIdCurso().equals(c.getIdCurso())) {
                VPrincipal.mostrarTextoError("\nYa existe un curso con el código proporcionado.");
                return;
            }
        }
        modelo.create(curso);
        VPrincipal.mostrarTexto("\nCurso dado de alta con éxito.");
    }
    
    private void read() {
        VCurso vc = new VCurso();
        HashSet hashset = modelo.readCurso();
        vc.mostrarVarios(hashset);
    }
    
    private void update() {
        VPrincipal.mostrarTexto("\nEscribe el código del curso que quieres modificar: ");
        String id = VPrincipal.leerTexto();
        Curso curso = null;
        HashSet cursos = modelo.readCurso();
        Iterator iterator = cursos.iterator();
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (id.equals(c.getIdCurso())) {
                curso = c;
            }
        }
        if (curso != null) {
            HashSet matriculas = modelo.readMatricula();
            iterator = matriculas.iterator();
            while (iterator.hasNext()) {
                Matricula m = (Matricula) iterator.next();
                if (curso.equals(m.getCurso())) {
                    VPrincipal.mostrarTextoError("\nEl curso que quieres actualizar tiene una matrícula asociada. "
                            + "Debes borrar primero la matrícula, actualizarlo y luego volver a crearla.");
                    return;
                }
            }
            IVista<Curso> vc = new VCurso();
            curso = vc.alta();
            curso.setIdCurso(id);
            modelo.update(curso);
            VPrincipal.mostrarTexto("\nCurso actualizado con éxito.");
        } else {
            VPrincipal.mostrarTextoError("\nEl código del curso proporcionado no existe.");
        }
    }
    
    private void delete() {       
        VPrincipal.mostrarTexto("\nIntroduce el código del curso que quieres borrar: ");
        String id = VPrincipal.leerTexto();
        Curso curso = null;
        HashSet cursos = modelo.readCurso();
        Iterator iterator = cursos.iterator();
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (id.equals(c.getIdCurso())) {
                curso = c;
            }
        }
        if (curso != null) {
            HashSet matriculas = modelo.readMatricula();
            iterator = matriculas.iterator();
            while (iterator.hasNext()) {
                Matricula m = (Matricula) iterator.next();
                if (curso.equals(m.getCurso())) {
                    VPrincipal.mostrarTextoError("\nEl curso que quieres borrar tiene una matrícula asociada. "
                            + "Debes borrar primero la matrícula.");
                    return;
                }
            }
        modelo.delete(curso);
        VPrincipal.mostrarTexto("\nCurso borrado con éxito.");
        } else {
            VPrincipal.mostrarTextoError("\nEl código del alumno proporcionado no existe.");
        }
    }
    
    private void search() {
        IVista<Curso> vc = new VCurso();
        Curso curso = null;
        VPrincipal.mostrarTexto("\nIntroduce el código del curso que quieres buscar: ");
        String id = VPrincipal.leerTexto();
        HashSet cursos = modelo.readCurso();
        Iterator iterator = cursos.iterator();
        
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (id.equals(c.getIdCurso())) {
                curso = c;
            }
        }
        if (curso != null) {
            VPrincipal.mostrarTexto("\n********************************************************************************");
            vc.mostrar(curso);
            VPrincipal.mostrarTexto("********************************************************************************");
        } else {
            VPrincipal.mostrarTextoError("\n¡Error! El código del curso proporcionado no existe.");
        }
    }
}