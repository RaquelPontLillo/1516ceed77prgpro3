package controlador;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Curso;
import modelo.Matricula;
import modelo.IModelo;
import vista.IVista;
import vista.VPrincipal;
import vista.VMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CMatricula {
    private IModelo modelo = null;
    
    public CMatricula(IModelo m) {
        modelo = m;
        Boolean bucle = true;
        
        do {
            char opcion = VPrincipal.menuCrud();  //Llamamos al menú CRUD

            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();  //Despedimos el CRUD y salimos de la función
                    return;
                case 'c':
                    create();       //Llamamos a la función PRIVADA create() del configurador
                    break;
                case 'r':
                    read();         //Llamamos a la función PRIVADA read() del configurador
                    break;
                case 'u':
                    update();       //Llamamos a la función PRIVADA update() del configurador
                    break;
                case 'd':
                    delete();       //Llamamos a la función PRIVADA delete() del configurador
                    break;
                case 's':
                    search();       //Llamamos a la función PRIVADA search() del configurador
                    break;
                default:
                    VPrincipal.errorMenu();     //Arrojamos error
                    break;
            }
        } while (bucle);
    }
    
    private void create() {
        /* 
         * El método create() tiene CARACTERÍSTICAS ADICIONALES programadas.
         * Pide al usuario los IDs del alumno y del curso de los que
         * desea formalizar una matrícula. Llama a los métodos getAlumno()
         * y getCurso(), definidos en el propio controlador, para COMPROBAR
         * SI EXISTEN LOS IDs. Si no existe alguno de los dos, arroja un 
         * error y NO PERMITE FORMALIZAR LA MATRÍCULA. 
         * 
         * Adicionalmente, este método NO PERMITE CREAR LA MATRÍCULA en el caso 
         * de que el ID que se proporcione para su creación YA ESTÉ SIENDO 
         * UTILIZADO. Garantiza IDs únicos en los vectores o colecciones.
        */
        IVista<Matricula> vm = new VMatricula();        //Creamos un objeto de la clase VMatricula a través de la interfaz IVista
        Matricula matricula = vm.alta();                //Creamos un objeto de la clase matrícula y llamamos al método de toma de datos alta()
        HashSet matriculas = modelo.readMatricula();    //Creamos un hashet de matrículas
        Iterator iterator = matriculas.iterator();      //Creamos un objeto Iterator para trabajar con el hashset
        while (iterator.hasNext()) {                    //Iteramos el hashset completo
            Matricula m = (Matricula) iterator.next();
            if (matricula.getIdMatricula().equals(m.getIdMatricula())) { //Si el ID que pone el usuario ya existe TERMINAMOS LA FUNCIÓN con un error
                VPrincipal.mostrarTextoError("\nYa existe una matricula con el código proporcionado.");     //Arrojamos error
                return;         //Terminamos la función, no permitimos crear una matrícula con un ID existente
            }
        }
        Alumno alumno = getAlumno();            //Obtenemos el alumno cuyo ID coincide con el dado por el usuario
        Curso curso = getCurso();               //Obtenemos el curso cuyo ID coincide con el dado por el usuario
        if (alumno!=null && curso!=null) {      //Comprobamos que los objetos no sean NULL
            matricula.setAlumno(alumno);        //Agregamos el objeto alumno al objeto matrícula
            matricula.setCurso(curso);          //Agregamos el objeto curso al objeto matrícula
            modelo.create(matricula);           //Creamos el objeto matrícula con todos los datos
            VPrincipal.mostrarTexto("\nMatrícula dada de alta con éxito");      //Mostramos mensaje de éxito
        } else {                                //Si el alumno o el curso resulta NULL, no permitimos que se cree la matrícula
            VPrincipal.mostrarTextoError("\n¡Error! El código de alumno o el código de curso proporcionados no existen."); //Arrojamos error
        }
    }
    
    private void read() {
        /*
         * El método read() permite iterar un hashset del objeto e ir mostrando
         * cada elemento de la colección.
        */
        VMatricula vm = new VMatricula();           //Creamos un objeto de la clase VMatricula
        HashSet hashset = modelo.readMatricula();   //Creamos un hashset de matrículas
        vm.mostrarVarios(hashset);                  //Llama al método mostrarVarios de VMatricula, que itera el hashet y muestra cada elemento
    }
    
    private void update() {
        /*
         * El método update() tiene CARACTERÍSTICAS ADICIONALES programadas.
         * Pide al usuario un ID de matrícula que modificar y los IDs del alumno y
         * curso por los que quiere modificar los existentes. Comprueba que los 
         * tres IDs proporcionados existen en el vector o colección de objetos y
         * en caso de que uno de ellos no exista NO PERMITE CREAR la matrícula.
        */
        VPrincipal.mostrarTexto("\nIntroduce el código de la matrícula que quieres actualizar: ");
        String idM = VPrincipal.leerTexto();        //Recogemos el ID de matrícula que nos da el usuario
        Alumno alumno = getAlumno();                //Recogemos el alumno cuyo ID nos da el usuario
        Curso curso = getCurso();                   //Recogemos el curso cuyo ID nos da el usuario
        Matricula matricula = null;                 //Creamos un objeto matrícula NULL
        HashSet matriculas = modelo.readMatricula();    //Creamos un hashset de matrículas
        Iterator iterator = matriculas.iterator();      //Creamos un objeto iterator para el hashset
        while (iterator.hasNext()) {                    //Iteramos el hashset de matrículas
            Matricula m = (Matricula) iterator.next();
            if (idM.equals(m.getIdMatricula())) {       //Si el ID proporcionado existe, nos guardamos la matrícula en el objeto matricula
                matricula = m;
            }
        }
        if (matricula!=null) {              //Comprobamos que el objeto matrícula no es NULL, en caso contrario DEVOLVEMOS ERROR
            if (alumno!=null && curso!=null) {  //Comprobamos que los objetos alumno y curso no son NULL, en caso contrario DEVOLVEMOS ERROR
                matricula.setAlumno(alumno);    //Agregamos el objeto alumno al objeto matrícula
                matricula.setCurso(curso);      //Agregamos el objeto curso al objeto matrícula
                modelo.update(matricula);       //Actualizamos el objeto matrícula
                VPrincipal.mostrarTexto("\nMatrícula actualizada con éxito.");  //Mostramos mensaje de éxito.
            } else {
                VPrincipal.mostrarTextoError("\n¡Error! El código de alumno o el código de curso proporcionados no existen."); //Arrojamos error
            }
        } else {
            VPrincipal.mostrarTextoError("\n¡Error! El código de la matrícula proporcionada no existe.");   //Arrojamos error
        }
    }
    
    private void delete() {
        /*
         * El método delete() tiene CARACTERÍSTICAS ADICIONALES programadas.
         * Pide un ID de matrícula a borrar al usuario, pero comprueba que exista
         * antes de borrarlo. Si no existe, arroja un mensaje de error.
        */
        VPrincipal.mostrarTexto("\nIntroduce el código de la matrícula que quieres borrar: ");
        String idM = VPrincipal.leerTexto();    //Recogemos el ID de matrícula que nos da el usuario
        Matricula matricula = null;             //Creamos un objeto matrícula NULL
        HashSet matriculas = modelo.readMatricula();    //Creamos un hashset de matrículas
        Iterator iterator = matriculas.iterator();      //Creamos un objeto iterator para el hashset
        while (iterator.hasNext()) {                    //Iteramos el hashset de matrículas
            Matricula m = (Matricula) iterator.next();  
            if (idM.equals(m.getIdMatricula())) {       //Si el ID proporcionado existe, nos guardamos la matrícula en el objeto matricula
                matricula = m;
            }
        }
        if (matricula != null) {                    //Comprobamos que la matrícula no sea NULL
            modelo.delete(matricula);               //Borramos la matrícula
            VPrincipal.mostrarTexto("\nMatrícula borrada con éxito.");  //Mostramos mensaje de éxito
        } else {
            VPrincipal.mostrarTextoError("\nEl código de la matrícula proporcionada no existe.");   //Arrojamos error
        }
    }
    
    private void search() {
        /*
         * Método extra (aportación) que permite buscar y mostrar una matrícula
         * concreta a partir de un ID que el usuario proporciona. Comprueba que 
         * el código de la matrícula exista, en caso contrario da error. 
         *
         * No se encuentra implementado en la interfaz IModelo por elección del 
         * programador pero PODRÍA ESTARLO y funcionar como un método más.
        */
        IVista<Matricula> vm = new VMatricula();    //Creamos un objeto VMatricula a partir de la interfaz IVista
        Matricula matricula = null;                 //Creamos un objeto matricula NULL
        VPrincipal.mostrarTexto("\nIntroduce el código de la matrícula que quieres buscar: ");
        String idM = VPrincipal.leerTexto();        //Recogemos el ID de matrícula que nos da el usuario
        HashSet matriculas = modelo.readMatricula(); //Creamos un hashset de matriculas
        Iterator iterator = matriculas.iterator();  //Creamos un objeto iterator para el hashset
        while (iterator.hasNext()) {                //Iteramos el hashset de matrículas
            Matricula m = (Matricula) iterator.next();
            if (idM.equals(m.getIdMatricula())) {   //Si el ID proporcionado existe, nos guardamos la matrícula en el objeto matricula
                matricula = m;
            }
        }
        if (matricula != null) {    //Comprobamos que la matricula no sea NULL, en caso contrario arrojamos mensaje de error
            vm.mostrar(matricula);  //Mostramos la matrícula
        } else {
            VPrincipal.mostrarTextoError("\n¡Error! El código de la matrícula proporcionada no existe."); //Arrojamos error
        }
    }
    
    private Alumno getAlumno() {
        /*
        * Este método sirve para obtener un alumno a partir de un vector o 
        * colección de alumnos. Si el ID no existe, el método devuelve un objeto
        * alumno NULL. Lo utilizan las funciones create() y update()
        */
        Alumno alumno = null;                            //Creamos un objeto alumno NULL
        VPrincipal.mostrarTexto("\nIntroduce el código del nuevo alumno: ");    
        String idA = VPrincipal.leerTexto();             //Recogemos el ID de alumno que nos da el usuario
        HashSet alumnos = modelo.readAlumno();          //Creamos un hashset de alumnos
        Iterator iterator = alumnos.iterator();         //Creamos un objeto iterator para el hashset        
        while (iterator.hasNext()) {                    //Iteramos el hashset
            Alumno a = (Alumno) iterator.next();
            if (idA.equals(a.getId())) {                //Si el ID proporcionado existe, nos guardamos el alumno en el objeto alumno
                alumno = a;
            }
        } 
        return alumno;                                  //Devolvemos el objeto alumno. Será NULL si el ID no existía
    }
    
    private Curso getCurso() {
       /*
        * Este método sirve para obtener un curso a partir de un vector o 
        * colección de cursos. Si el ID no existe, el método devuelve un objeto
        * curso NULL. Lo utilizan las funciones create() y update()
        */
        Curso curso = null;                           //Creamos un objeto curso NULL
        VPrincipal.mostrarTexto("\nIntroduce el código del nuevo curso: ");
        String idC = VPrincipal.leerTexto();         //Recogemos el ID de curso que nos da el usuario
        HashSet cursos = modelo.readCurso();         //Creamos un hashset de cursos
        Iterator iterator = cursos.iterator();       //Creamos un objeto iterator para el hashset
        while (iterator.hasNext()) {                 //Iteramos el hashset
            Curso c = (Curso) iterator.next();
            if (idC.equals(c.getIdCurso())) {       //Si el ID proporcionado existe, nos guardamos el curso en el objeto curso
                curso = c;
            }
        }
        return curso;                               //Devolvemos el objeto curso. Será NULL si el ID no existía
    }
}