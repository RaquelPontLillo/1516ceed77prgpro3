package controlador;

import modelo.IModelo;
import modelo.ModeloArray;
import modelo.ModeloHashSet;
import vista.VPrincipal;
import java.util.InputMismatchException;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CPrincipal {
    IModelo modelo = null;
    VPrincipal vista = new VPrincipal();
    
    public CPrincipal (IModelo m, VPrincipal v) throws InputMismatchException {
        vista = v;
        modelo = m;
        Boolean bucle = true;
        
        do {
            try {
                char opt = VPrincipal.menuEstructura();
                switch (opt) {
                    case 's':
                        VPrincipal.despedir();
                        return;
                    case 'a':
                        modelo = new ModeloArray();
                        bucle = false;
                        break;
                    case 'h':
                        modelo = new ModeloHashSet();
                        bucle = false;
                        break;
                    default:
                        VPrincipal.errorMenu();
                        break;
                }
            } catch (InputMismatchException e) {
                VPrincipal.errorNum();
            }
        } while (bucle);
        
        bucle = true;
        
        do {
            try {
                int opt = VPrincipal.menuInicial();
                switch (opt) {
                    case 0:
                        VPrincipal.despedir();
                        return;
                    case 1:
                        CAlumno ca = new CAlumno(modelo);
                        break;
                    case 2:
                        CCurso cc = new CCurso(modelo);
                        break;
                    case 3:
                        CMatricula cm = new CMatricula(modelo);
                        break;
                    default:
                        VPrincipal.errorMenu();
                        break;
                }
            } catch (InputMismatchException e) {
                VPrincipal.errorNum();
            }
        } while (bucle);
  }
}
