package modelo;

import java.util.HashSet;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloHashSet implements IModelo {
    private HashSet<Alumno> alumnos = new HashSet();
    private HashSet <Curso> cursos = new HashSet(); 
    private HashSet <Matricula> matriculas = new HashSet();

    @Override
    public void create(Alumno alumno) {
        alumnos.add(alumno);
    }
    
    @Override
    public void create(Curso curso) {
        cursos.add(curso);
    }
    
    @Override
    public void create(Matricula matricula) {
        matriculas.add(matricula);
    }

    @Override
    public HashSet readAlumno() {
        return alumnos;
    }

    @Override
    public HashSet readCurso() {
        return cursos;
    }
    
    @Override
    public HashSet readMatricula() {
        return matriculas;
    }
    
    @Override
    public void update(Alumno alumno) {
        Alumno toRemove = null;
        for (Alumno a: alumnos) {
            if(a.getId().equals(alumno.getId())) {
                toRemove = a;
            }
        }
        alumnos.remove(toRemove);
        alumnos.add(alumno);
    }
    
    @Override
    public void update(Curso curso) {
        Curso toRemove = null;
        for (Curso c: cursos) {
            if(c.getIdCurso().equals(curso.getIdCurso())) {
                toRemove = c;
            }
        }
        cursos.remove(toRemove);
        cursos.add(curso);
    }
    
    @Override
    public void update(Matricula matricula) {
        Matricula toRemove = null;
        for (Matricula m: matriculas) {
            if(m.getIdMatricula().equals(matricula.getIdMatricula())) {
                toRemove = m;
            }
        }
        matriculas.remove(toRemove);
        matriculas.add(matricula);
    }

    @Override
    public void delete(Alumno alumno) {
        Alumno toRemove = null;
        for (Alumno a: alumnos) {
            if (a.getId().equals(alumno.getId())) {
                toRemove = a; 
            }
        }
        alumnos.remove(toRemove);
    }
    
    @Override
    public void delete(Curso curso) {
        Curso toRemove = null;
        for (Curso c: cursos) {
            if (c.getIdCurso().equals(curso.getIdCurso())) {
                toRemove = c;
            }
        }
        cursos.remove(toRemove);
    }
    
    @Override
    public void delete(Matricula matricula) {
        Matricula toRemove = null;
        for(Matricula m: matriculas) {
            if (m.getIdMatricula().equals(matricula.getIdMatricula())) {
                toRemove = m;
            }
        }
        matriculas.remove(toRemove);
    }
}